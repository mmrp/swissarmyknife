# **(1) Installare Laravel (dalla dir homestead: Code/ )** #

```
#!bash
composer create-project --prefer-dist laravel/laravel <nuovo_progetto> "5.3.*"
```

```
#!bash
cd <nuovo_progetto>
```

```
#!bash
php artisan make:auth
```

# **(2) Installare SwissArmyKnife** #

```
#!bash
composer require mmrp/swissarmyknife dev-master
```

aggiungere in config/app.php


```
#!php
Mmrp\Swissarmyknife\SwissArmyKnifeProvider::class,
```

eseguire


```
#!bash
php artisan vendor:publish  --force 
```

aggiungere in Http/Kernel.php nelle route middleware la seguente linea:

```
#!php
'permissions'=> \App\Http\Middleware\Permissions::class
```


# ***(2.01) BUGFIX*** #
Questi passi sono da eseguire per risolvere dei problemi. In una versione
finale, non dovrebbero essere eseguiti.
    
* spostare il file **permissions.json** in storage/
* modificare il file **web.php** all'interno della linea **25** rimuovere 

```
#!php
,'permissions'
```

* modificare il file **web.php** commentando le singole linee: **33**,**58**
* modificare il file **sidebar.blade.php** commentando le singole linee: **38**,**40**
    

# *(2.1) OPZIONALE* #
Utilizzo di un database specifico. 
Per utilizzare un database specifico è necessario andare a modificare il 
file **.env** nella root del progetto. 
          
```
#!bash

 DB_DATABASE=<nome_db_nuovo_progetto>
```

quindi, prima di testare l'applicazione è necessario creare un db che 
abbia lo stesso nome.

# **(3) Migrations:** #

```
#!bash
composer dump-autoload;
```  

```
#!bash
php artisan migrate
```