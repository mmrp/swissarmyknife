<!-- Left side column. contains the logo and sidebar -->
@if(!Auth::guest())
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->

        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ (Auth::user()->profile_image) ? Auth::user()->profile_image : asset('/img/avatar5.png') }}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('messages.online') }}</a>
            </div>
        </div>

        <!-- search form (Optional) -->
        <form action="" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="{{ trans('messages.search') }}" value="{{ Request::input('q') }}"/>
            <span class="input-group-btn">
                <button type='submit' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
            </span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">

            <li class="header">{{ trans('messages.settings') }}</li>
            <!-- Optionally, you can add icons to the links -->
            <li class="{{ (isset($resource) and $resource == 'user') ? 'active' : '' }}"><a href="{{ action('Management\UserController@index') }}"><i class='fa fa-users'></i> <span>{{ trans('user.users') }}</span></a></li>
            @if(Auth::user()->role == 'superuser')
                <li class="{{ (isset($resource) and $resource == 'log') ? 'active' : '' }}"><a href="{{ action('Management\LogController@index') }}"><i class='fa fa-archive'></i> <span>{{ trans('log.logs') }}</span></a></li>
            @endif
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
@endif