<?php

return [
    'users' => 'Utenti',
    'user' => 'Utente',

    'id' => '#',
    'name' => 'Nome',
    'email' => 'E-Mail',
    'password' => 'Password',
    'role_id' => 'Ruolo',
    'profile_image' => 'Foto Profilo',
    'client_api_token' => 'Token API',
    'client_token_api_last_update' => 'Token API Aggiornato Il',
];