<?php
return [
    'user' => 'User',
    'users' => 'Users',

    'id' => '#',
    'name' => 'Name',
    'email' => 'E-Mail',
    'password' => 'Password',
    'role' => 'Role',
    'profile_image' => 'Profile Image',
    'api_token' => 'Token API',
    'api_token_last_update' => 'Token API Last Update',



];