<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Permissions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $action = str_replace('App\Http\Controllers\\','',$request->route()->getActionName());
        $class = explode('@',$action)[0];

        $permissions = $this->permissions(Auth::user()->role);

        if ( Auth::check() && (isset($permissions['*@*']) or isset($permissions[$action]) or isset($permissions[$class . '@*'])) )
        {
            return $next($request);
        }

        abort(403);
    }

    private function permissions($role)
    {
        $json = $this->openJSON();

        if(!isset($json[$role])){
            abort(403);
        }

        return $json[$role]['permissions'];
    }

    private function openJSON()
    {
        try{
            return json_decode(file_get_contents(storage_path('permissions.json')),true);
        }
        catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
}
