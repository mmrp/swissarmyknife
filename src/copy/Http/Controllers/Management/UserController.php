<?php

namespace App\Http\Controllers\Management;

use App\Mail\RegisterUser;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rule;
use Mmrp\Swissarmyknife\Controller\BaseCrudController;

class UserController extends BaseCrudController
{
    protected $action = NULL;
    protected $resource = NULL;
    protected $views_folder = NULL;
    protected $title = NULL;
    protected $model = NULL;
    protected $batch_import_model = NULL;
    protected $batch_import_log = NULL;
    protected $validation_rules = NULL;
    protected $breadcrumbs = NULL;

    public function __construct(Request $request)
    {
        $this->action = 'Management\UserController';
        $this->resource = 'user';
        $this->views_folder = 'crud';
        $this->model = new User();
        $this->validation_rules = [
            'name' => ['required', 'string'],
            'email' => ['required', 'email', Rule::unique('users')->ignore($request->route()->parameter('id'))],
            'role' => ['required'],
            'profile_image' => ['image']
        ];

        $this->breadcrumbs = [];
        $this->title = '';

        $this->fields_types = [
            'role' => [
                makeFieldSelect([
                    'superuser' => 'Superuser',
                    'administrator' => 'Administrator',
                    'partner' => 'Partner',
                    'client_api' => 'Client API',
                ]),
            ],
            'profile_image' => [ makeFieldProfileImage() ],
            'password' => [makeFieldHidden()],
        ];

        parent::__construct($request);

    }

    protected function prepareIndex(Request $request)
    {
        $this->title = '<i class="fa fa-fw fa-users"></i> ' . trans('user.users');
        $this->breadcrumbs = [
            ['link' => '#', 'title' => trans('user.users'), 'active' => TRUE],
        ];
        $this->fields_types['profile_image'] = [makeFieldHidden()];
    }

    protected function prepareGet(Request $request, $id)
    {

        $this->model = $this->model->findOrFail($id);

        $this->title = '<i class="fa fa-fw fa-users"></i> ' . trans('user.user') . ':' . $this->model->email;

        $this->breadcrumbs = [
            ['link' => action($this->action . '@index'), 'title' => trans('user.users')],
            ['link' => '#', 'title' => $this->model->email, 'active' => TRUE],
        ];
    }

    protected function prepareInsert(Request $request)
    {
        $this->fields = array_merge($this->fields, ['password']);
        $this->title = '<i class="fa fa-fw fa-users"></i>' . trans('user.user');
        $this->breadcrumbs = [
            ['link' => action($this->action . '@index'), 'title' => trans('user.users')],
            ['link' => '#', 'title' => trans('messages.button.new'), 'active' => TRUE],
        ];

    }

    protected function prepareEdit(Request $request, $id)
    {
        $this->fields = array_merge($this->fields, ['password']);
        $this->title = '<i class="fa fa-fw fa-users"></i>' . trans('user.user');
        $this->breadcrumbs = [
            ['link' => action($this->action . '@index'), 'title' => trans('user.users')],
            ['link' => action($this->action . '@get', ['id' => $this->model->id]), 'title' => $this->model->email,],
            ['link' => '#', 'title' => trans('messages.button.edit'), 'active' => TRUE],
        ];
    }

    protected function prepareTrash(Request $request)
    {
        $this->title = '<i class="fa fa-fw fa-trash"></i> ' . trans('messages.trash') . ' ' . trans('user.users');
        $this->breadcrumbs = [
            ['link' => action($this->action . '@index'), 'title' => trans('user.users')],
            ['link' => '#', 'title' => trans('messages.trash'), 'active' => TRUE],
        ];
    }

    protected function beforeSave(Request $request, $id = NULL)
    {
        if (is_null($id)) {
            $this->password = str_random(32);
            $this->input['password'] = bcrypt($this->password);
        }

        if (!empty($this->input['client_api_token'])) {
            $this->input['client_api_token_last_update'] = Carbon::now();
        }

        if (!empty($this->input['profile_image'])) {
            $this->input['profile_image'] = 'data:' . $this->input['profile_image']->getMimeType() . ';base64,' . base64_encode(file_get_contents($this->input['profile_image']));
        }
    }

    protected function afterSave(Request $request, $id = NULL)
    {
        $user = new \stdClass();
        $user->name = $this->model->name;
        $user->email = $this->model->email;
        $user->password = $this->password;

        Mail::to($this->model->email)->send(new RegisterUser($user));
    }
}