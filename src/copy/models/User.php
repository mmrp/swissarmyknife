<?php
/**
 * Created by PhpStorm.
 * User: Matteo Meloni
 * Date: 07/11/2016
 * Time: 12:00
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'profile_image','name','password','email','role',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
}