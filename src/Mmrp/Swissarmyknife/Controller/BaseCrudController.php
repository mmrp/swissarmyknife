<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 11/04/17
 * Time: 14:54
 */

namespace Mmrp\Swissarmyknife\Controller;


use App\Http\Controllers\Controller;

use Mmrp\Swissarmyknife\Lib\Log;

use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

/**
 * Class BaseCrudController
 * @package App\Http\Controllers
 */
class BaseCrudController extends Controller
{
    /**
     * Set if change default redirect action
     * @var null
     */
    protected $redirect_to = NULL;

    /**
     * Namaspace of child Controller (used to create the routes)
     * @example Management\UserController
     * @var string
     */
    protected $action = NULL;

    /**
     * Url parameters
     * @example [ 'key' => 'value' ]
     * @var array
     */
    protected $parameters = [];

    /**
     * String that contains the name of the resource used
     * @var string
     */
    protected $resource = NULL;

    /**
     * String that contains the path of views folder
     * @var string
     */
    protected $views_folder = NULL;

    /**
     * String that contains page title
     * @var string
     */
    protected $title = NULL;

    /**
     * String that contains page subtitle
     * @var string
     */
    protected $subtitle = NULL;

    /**
     * Array that contains list of model fields: use fillable attribute belonging to current model
     * @var array
     */
    protected $fields = NULL;

    /**
     * Array that contains fields types
     * @example [ 'password' => [ makeFieldPassword() ] ]
     * @link swissarmyknife helper guide
     * @var array
     */
    protected $fields_types = NULL;
    /**
     * Contains instance of EloquentORM which is used to interact with request table
     * @var Model
     */
    protected $model = NULL;

    /**
     * Array that contains request inputs
     * @var array
     */
    protected $input = NULL;

    /**
     * Array that contains laravel validation rules
     * @var array
     */
    protected $validation_rules = NULL;

    /**
     * Used to create breadcrumbs
     * @var array
     */
    protected $breadcrumbs = NULL;

    /**
     * Used to pass additional data to custom views
     * @var mixed
     */
    protected $additional_data = NULL;

    /**
     * Used to enable/disable fields translations
     * @var bool
     */
    protected $translate_fields = TRUE;

    /**
     * Used to enable/disable index() method
     * @var bool
     */
    protected $index = TRUE;

    /**
     * Used to enable/disable search() method
     * @var bool
     */
    protected $search = FALSE;

    /**
     * Used to enable/disable get() method
     * @var bool
     */
    protected $get = TRUE;

    /**
     * Used to enable/disable insert() method
     * @var bool
     */
    protected $insert = TRUE;

    /**
     * Used to enable/disable edit() method
     * @var bool
     */
    protected $edit = TRUE;

    /**
     * Used to enable/disable delete() method
     * @var bool
     */
    protected $delete = TRUE;

    /**
     * Used to enable/disable destroy() method
     * @var bool
     */
    protected $destroy = TRUE;

    /**
     * Used to enable/disable restore() method
     * @var bool
     */
    protected $restore = TRUE;

    /**
     * Used to enable/disable trash() method
     * @var bool
     */
    protected $trash = TRUE;

    /**
     * Used to enable/disable save() method
     * @var bool
     */
    protected $save = TRUE;

    /**
     * Used to enable/disable download() method
     * @var bool
     */
    protected $download = TRUE;

    /**
     * Used to enable/disable multiple_operations at index/trash view
     * @var bool
     */
    protected $multiple_operations = TRUE;

    /**
     * Array that contains a list of custom actions
     * @example [ 'link' => 'mylink', 'title' => 'MyCustomAction' ]
     * @var null
     */
    protected $custom_action = NULL;

    /**
     * BaseCrudController constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->fields = (array_diff($this->model->getFillable(), $this->model->getHidden()));
        $this->views_folder = 'crud';
    }

    /**
     * Return Index View
     * @param Request $request
     * @return View
     */
    public function index(Request $request)
    {
        if(!$this->index){
            abort(501);
        }

        try {
            $this->model = $this->filterAndOrder($request, $this->model);
            $this->model = $this->addFilter($request,$this->model);

            if($request->input('per_page') == 'all'){
                $this->model = $this->model->get();
            } else {
                $this->model = $this->model->paginate(($request->input('per_page')) ? $request->input('per_page') : 25);
                $this->model->appends($request->input())->links();
            }

            $this->prepareIndex($request);

            Log::info(new \Exception('getIndex', 200), $request,
                [
                    'action' => 'getIndex',
                    'resource' => $this->resource,
                ]
            );

            return view(preg_match('/index/',$this->views_folder) ? $this->views_folder : $this->views_folder . '.index')
                ->with('action', $this->action)
                ->with('parameters', $this->parameters)
                ->with('resource', $this->resource)
                ->with('title', $this->title)
                ->with('fields', $this->fields)
                ->with('translate_fields', $this->translate_fields)
                ->with('fields_types', $this->fields_types)
                ->with('data', $this->model)
                ->with('additional_data',$this->additional_data)
                ->with('breadcrumbs',$this->breadcrumbs)
                ->with('available_action', $this->prepareAction())
                ->with('custom_actions', $this->custom_action)
                ->with('batch_import', method_exists($this,'initBatchImportTrait'));
        }
        catch (\Exception $e) {
            Log::info($e, $request, [
                    'action' => 'getIndex',
                    'resource' => $this->resource,
                ]
            );

            return redirect()->back();
        }
    }

    /**
     * Return Search View
     * @param Request $request
     * @param $needle string
     * @return View
     */
    public function search(Request $request)
    {
        if(!$this->search){
            abort(501);
        }

        $this->insert = FALSE;
        $this->edit = FALSE;
        $this->trash = FALSE;
        $this->delete = FALSE;
        $this->destroy = FALSE;

        $needle = $request->input('q');

        try {
            foreach ($this->fields as $field){
                $this->model = $this->model->orWhere($field,'like','%' . $needle .'%');
            }

            $this->model = $this->addFilter($request,$this->model);
            $this->model = $this->model->paginate(($request->input('per_page')) ? $request->input('per_page') : 25);

            $this->prepareIndex($request);

            Log::info(new \Exception('getIndex', 200), $request,
                [
                    'action' => 'getIndex',
                    'resource' => $this->resource,
                ]
            );

            return view(preg_match('/index/',$this->views_folder) ? $this->views_folder : $this->views_folder . '.index')
                ->with('action', $this->action)
                ->with('parameters', $this->parameters)
                ->with('resource', $this->resource)
                ->with('title', $this->title)
                ->with('fields', $this->fields)
                ->with('translate_fields', $this->translate_fields)
                ->with('fields_types', $this->fields_types)
                ->with('data', $this->model)
                ->with('breadcrumbs',$this->breadcrumbs)
                ->with('available_action', $this->prepareAction())
                ->with('batch_import', method_exists($this,'initBatchImportTrait'));
        }
        catch (\Exception $e) {
            Log::info($e, $request, [
                    'action' => 'getIndex',
                    'resource' => $this->resource,
                ]
            );

            return redirect()->back();
        }
    }

    /**
     * Return list of retrieved objects
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function async(Request $request)
    {
        try {
            $this->prepareAsync($request);
            $this->model = $this->addFilter($request,$this->model);
            $this->model = $this->model->paginate(($request->input('per_page')) ? $request->input('per_page') : 25);

            Log::info(new \Exception('getAsync'.ucfirst($this->resource), 200), $request,
                [
                    'action' => 'getAsync',
                    'resource' => $this->resource,
                ]
            );

            $empty = new \stdClass();
            $empty->id = "null";
            $empty->text = '--';
            $this->model->push($empty);

            return response()->json($this->model);
        }
        catch (\Exception $e) {
            Log::info($e, $request, [
                    'action' => 'getAsync',
                    'resource' => $this->resource,
                ]
            );

            return redirect()->back();
        }
    }

    /**
     * Return View that contains a specified row
     * @param Request $request
     * @param $id
     * @return View
     */
    public function get(Request $request, $id)
    {
        if(!$this->get){
            abort(501);
        }

        $id = $request->route()->getParameter('id');

        try {
            $this->model = $this->model->findOrFail($id);

            $this->prepareGet($request, $id);

            Log::info(new \Exception('get',200), $request, [
                    'action' => 'get',
                    'resource' => $this->resource,
                    'resource_id' => $id
                ]
            );

            return view(preg_match('/show/',$this->views_folder) ? $this->views_folder : $this->views_folder . '.show')
                ->with('action',$this->action)
                ->with('parameters', $this->parameters)
                ->with('resource', $this->resource)
                ->with('title', $this->title)
                ->with('fields', $this->fields)
                ->with('translated_fields', $this->translate_fields)
                ->with('fields_types', $this->fields_types)
                ->with('data', $this->model)
                ->with('additional_data',$this->additional_data)
                ->with('available_action', $this->prepareAction())
                ->with('custom_actions', $this->custom_action)
                ->with('breadcrumbs', $this->breadcrumbs);
        }
        catch (\Exception $e) {
            Log::info($e, $request, [
                    'action' => 'get',
                    'resource' => $this->resource,
                    'resource_id' => $id
                ]
            );

            return redirect()->back();
        }
    }

    /**
     * Return Insert Form View
     * @param Request $request
     * @return View
     */
    public function insert(Request $request)
    {
        if(!$this->insert){
            abort(501);
        }

        try {
            $this->prepareInsert($request);

            return view(preg_match('/edit/',$this->views_folder) ? $this->views_folder : $this->views_folder . '.edit')
                ->with('action', $this->action)
                ->with('parameters', $this->parameters)
                ->with('resource', $this->resource)
                ->with('title', $this->title)
                ->with('fields', $this->fields)
                ->with('fields_types', $this->fields_types)
                ->with('translate_fields', $this->translate_fields)
                ->with('available_action', $this->prepareAction())
                ->with('additional_data',$this->additional_data)
                ->with('breadcrumbs', $this->breadcrumbs);
        }
        catch (\Exception $e) {
            Log::info($e, $request, [
                    'action' => 'insert',
                    'resource' => $this->resource,
                ]
            );

            return redirect()->back();
        }
    }

    /**
     * Return Edit Form View
     * @param Request $request
     * @param $id
     * @return View
     */
    public function edit(Request $request, $id)
    {
        if(!$this->edit){
            abort(501);
        }

        $id = $request->route()->getParameter('id');

        try {
            $this->model = $this->model->findOrFail($id);

            $this->prepareEdit($request, $id);

            Log::info(new \Exception('edit', 200), $request, [
                    'action' => 'edit',
                    'resource' => $this->resource,
                    'resource_id' => $id
                ]
            );

            return view(preg_match('/edit/',$this->views_folder) ? $this->views_folder : $this->views_folder . '.edit')
                ->with('action', $this->action)
                ->with('parameters', $this->parameters)
                ->with('resource', $this->resource)
                ->with('title', $this->title)
                ->with('fields', $this->fields)
                ->with('fields_types', $this->fields_types)
                ->with('translate_fields', $this->translate_fields)
                ->with('data', $this->model)
                ->with('additional_data',$this->additional_data)
                ->with('available_action', $this->prepareAction())
                ->with('breadcrumbs', $this->breadcrumbs);
        }
        catch (\Exception $e) {
            Log::info($e, $request, [
                    'action' => 'getEdit',
                    'resource' => $this->resource,
                    'resource_id' => $id
                ]
            );

            return redirect()->back();
        }
    }

    /**
     * Delete the specified line
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request, $id)
    {
        if(!$this->delete){
            abort(501);
        }

        $id = $request->route()->getParameter('id');

        try{
            $this->beforeDelete($request, $id);
            if($id == 'multiple'){
                $this->model->whereIn('id',$request->input('rows_id'))->delete();
            } else {
                $this->model->where('id',$id)->first()->delete();
            }

            $this->afterDelete($request, $id);

            Session::flash('flash_message', trans('messages.edit.deleted'));

            if(!is_null($this->redirect_to)){
                return redirect()->to($this->redirect_to);
            } else {
                return redirect()->action($this->action . '@index', $this->parameters);
            }
        }
        catch (\Exception $e) {
            Log::info($e, $request, [
                    'action' => 'delete',
                    'resource' => $this->resource,
                    'resource_id' => $id
                ]
            );

            return redirect()->back();
        }
    }

    /**
     * Destroy the specified line
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, $id)
    {
        if(!$this->destroy){
            abort(501);
        }

        $id = $request->route()->getParameter('id');

        try {
            $this->beforeDestroy($request, $id);

            if($id == 'multiple'){
                $this->model->withTrashed()->whereIn('id',$request->input('rows_id'))->forceDelete();
            } else {
                $this->model->withTrashed()->where('id', $id)->forceDelete();
            }

            $this->afterDestroy($request,$id);

            Session::flash('flash_message', trans('messages.edit.destroyed'));

            if(!is_null($this->redirect_to)){
                return redirect()->to($this->redirect_to);
            } else {
                return redirect()->action($this->action . '@trash', $this->parameters);
            }

        }
        catch (\Exception $e) {
            Log::info($e, $request, [
                    'action' => 'destroy',
                    'resource' => $this->resource,
                    'resource_id' => $id
                ]
            );

            return redirect()->back();
        }
    }

    /**
     * Restore the specified line
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore(Request $request, $id)
    {
        if(!$this->restore){
            abort(501);
        }

        $id = $request->route()->getParameter('id');

        try {
            $this->beforeRestore($request, $id);

            if($id == 'multiple'){
                $this->model->withTrashed()->whereIn('id',$request->input('rows_id'))->restore();
            } else {
                $this->model->withTrashed()->where('id', $id)->restore();
            }

            $this->afterRestore($request, $id);

            Session::flash('flash_message', trans('messages.edit.restored'));

            if(!is_null($this->redirect_to)){
                return redirect()->to($this->redirect_to);
            } else {
                return redirect()->action($this->action . '@trash', $this->parameters);
            }

        }
        catch (\Exception $e) {
            Log::info($e, $request, [
                    'action' => 'restore',
                    'resource' => $this->resource,
                    'resource_id' => $id
                ]
            );

            return redirect()->back();
        }
    }

    /**
     * Return Trash View
     * @param Request $request
     * @return View
     */
    public function trash(Request $request)
    {
        if(!$this->trash){
            abort(501);
        }

        try {
            $this->model = $this->filterAndOrder($request, $this->model);
            $this->model = $this->addFilter($request,$this->model);
            $this->model = $this->model->orderBy('id')->onlyTrashed();

            if($request->input('per_page') == 'all'){
                $this->model = $this->model->get();
            } else {
                $this->model = $this->model->paginate(($request->input('per_page')) ? $request->input('per_page') : 25);
                $this->model->appends($request->input())->links();
            }

            $this->prepareTrash($request);

            Log::info(new \Exception('trash', 200), $request,
                [
                    'action' => 'trash',
                    'resource' => $this->resource,
                ]
            );
            return view(preg_match('/index/',$this->views_folder) ? $this->views_folder : $this->views_folder . '.index')
                ->with('trash', TRUE)
                ->with('action',$this->action)
                ->with('parameters', $this->parameters)
                ->with('resource', $this->resource)
                ->with('title', $this->title)
                ->with('fields', $this->fields)
                ->with('translate_fields', $this->translate_fields)
                ->with('fields_types', $this->fields_types)
                ->with('data', $this->model)
                ->with('additional_data',$this->additional_data)
                ->with('available_action', $this->prepareAction())
                ->with('breadcrumbs',$this->breadcrumbs);
        }
        catch (\Exception $e) {
            Log::info($e, $request, [
                    'action' => 'trash',
                    'resource' => $this->resource,
                ]
            );

            return redirect()->back();
        }
    }

    /**
     * Creates a new row or Fill specified row
     * @param Request $request
     * @param null $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Request $request, $id = NULL)
    {
        $id = $request->route()->getParameter('id');

        if(!$this->save){
            abort(501);
        }

        $this->input = $request->except('_token');

        $request->replace($this->input);

        try{
            $this->validate($request, $this->validation_rules);
        }
        catch (\Exception $e){
            Log::info($e, $request, [
                    'action' => 'save',
                    'resource' => $this->resource,
                ]
            );

            return redirect()->back();
        }

        foreach ($this->input as $key => $value) {
            if($value == 'null'){
                $this->input[$key] = NULL;
            }
        }

        $this->beforeSave($request, $id);

        try {
            $this->_save($id);

            $this->afterSave($request, $id);

            Session::flash('flash_message', ($id) ? trans('messages.edit.updated') : trans('messages.edit.inserted'));

            Log::info(new \Exception(($id) ? trans('messages.edit.updated') : trans('messages.edit.inserted'), 200), $request,
                [
                    'action' => 'save',
                    'resource' => $this->resource,
                    'resource_id' => ($id) ? $id : $this->model->id
                ]
            );

            if(!is_null($this->redirect_to)){
                return redirect()->to($this->redirect_to);
            } else {
                return redirect()->action($this->action . '@get', array_merge($this->parameters, ['id' => $this->model->id]));
            }

        }
        catch (\Exception $e) {
            Session::flash('flash_message', ($id) ? trans('messages.edit.failed') : trans('messages.edit.failed'));

            Log::info($e, $request, [
                    'action' => 'save',
                    'resource' => $this->resource,
                ]
            );

            return redirect()->back();
        }
    }

    /**
     * Donwload request file
     * @param Request $request
     * @param null $path
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function download(Request $request, $path = NULL)
    {
        if(!$this->index){
            abort(501);
        }

        try{
            Log::info(new \Exception('download', 200), $request,
                [
                    'action' => 'download',
                    'resource' => $this->resource,
                ]
            );

            $this->beforeDownload($request, $path);

            return response()->download(storage_path(base64_decode($path)));
        }
        catch (\Exception $e){
            Log::info(new \Exception('download', 200), $request,
                [
                    'action' => 'download',
                    'resource' => $this->resource,
                ]
            );

            return redirect()->back();
        }
    }

    /**
     * Called by child class, prepare @index environment
     * @param Request $request
     */
    protected function prepareIndex(Request $request)
    {

    }

    /**
     * Called by child class, prepare @async environment
     * @param Request $request
     */
    protected function prepareAsync(Request $request)
    {
        $this->model = $this->model->select('id', 'name as text');
        $this->model = $this->filterAndOrder($request, $this->model);
        $this->model = $this->model->orderBy('name');
    }

    /**
     * Called by child class, prepare @get environment
     * @param Request $request
     * @param $id
     */
    protected function prepareGet(Request $request, $id)
    {

    }

    /**
     * Called by child class, prepare @insert environment
     * @param Request $request
     */
    protected function prepareInsert(Request $request)
    {

    }

    /**
     * Called by child class, prepare @edit environment
     * @param Request $request
     * @param $id
     */
    protected function prepareEdit(Request $request, $id)
    {

    }

    /**
     * Called by child class, execute your code before $this->model->delete()
     * @param Request $request
     * @param $id
     */
    protected function beforeDelete(Request $request, $id)
    {

    }

    /**
     * Called by child class, executed after $this->model->delete()
     * @param Request $request
     * @param $id
     */
    protected function afterDelete(Request $request, $id)
    {

    }

    /**
     * Called by child class, executed before $this->model->destroy()
     * @param Request $request
     * @param $id
     */
    protected function beforeDestroy(Request $request, $id)
    {

    }

    /**
     * Called by child class, executed before $this->model->destroy()
     * @param Request $request
     * @param $id
     */
    protected function afterDestroy(Request $request, $id)
    {

    }

    /**
     * Called by child class, executed before $this->model->restore()
     * @param Request $request
     * @param $id
     */
    protected function beforeRestore(Request $request, $id)
    {

    }

    /**
     * Called by child class, executed before $this->model->restore()
     * @param Request $request
     * @param $id
     */
    protected function afterRestore(Request $request, $id)
    {

    }

    /**
     * Called by child class, prepare @trash environment
     * @param Request $request
     */
    protected function prepareTrash(Request $request)
    {

    }

    /**
     * Save/Fill Model Object with $this->input values
     * @param $id
     */
    protected function _save($id)
    {
        if ($id) {
            $this->model = $this->model->findOrFail($id);
            $this->model->forceFill($this->input)->save();
        } else {
            foreach ($this->input as $field => $value){
                $this->model->$field = $value;
            }

            $this->model->save();
        }
    }

    /**
     * Called by child class, prepare @save environment
     * @param Request $request
     * @param null $id
     */
    protected function beforeSave(Request $request, $id = NULL)
    {

    }

    /**
     * Called by child class, execute operation after save
     * @param Request $request
     * @param null $id
     */
    protected function afterSave(Request $request, $id = NULL)
    {

    }

    /**
     * Called by child class, execute operation before downloaded
     * @param Request $request
     * @param null $path
     */
    protected function beforeDownload(Request $request, $path = NULL)
    {

    }

    /**
     * Array that contains granted operations
     * @return array
     */
    protected function prepareAction()
    {
        return [
            'index' => $this->index,
            'get' => $this->get,
            'insert' => $this->insert,
            'edit' => $this->edit,
            'delete' => $this->delete,
            'destroy' => $this->destroy,
            'restore' => $this->restore,
            'trash' => $this->trash,
            'save' => $this->save,
            'download' => $this->download,
            'multiple_operations' => $this->multiple_operations
        ];
    }
}
